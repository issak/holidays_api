# 节假日api

#### 介绍
免费节假日API 开源了,可以离线调用, 可以集成到自己系统中

#### 开源说明

注:原百度节假日API即为本人提供,后百度apistore禁止个人开发者所以才有此独立接口.
鉴于免费版访问量过大对于服务器压力过大现将免费版开源供大家离线调用.

开源的功能为最核心的内容.大家可自行根据需要进行二次开发.本次开源不提供技术指导.如需要二次开发请联系我定制(200元起)

仅有php版本,其它语言可以参考代码自行开发

免费api和vip接口将继续为大家服务.

免费api:http://tool.bitefu.net/jiari/
VIP通道:http://tool.bitefu.net/jiari/vip.php
VIP专用通道:http://vip.bitefu.net/jiari/

免费节假日api开源版(支持批量查询)

下载地址1::http://www.90pan.com/b1458379

下载地址2:https://download.csdn.net/download/ganggang4321/10962182

#### 使用说明

1. 下载源码部署到到系统中直接调用?d=日期
2. 离线调用方法

```
 include('include/dateapi.class.php');
 $api= new dateapi();
 $result= $api->getday($date);
```

#### 捐助我

捐助列表:http://tool.bitefu.net/pay/

<a href="http://tool.bitefu.net/pay/alipayred.jpg" target="_blank"><img height="300" src="https://oscimg.oschina.net/oscnet/69e93259cb3fa4f12a2c4e96dfc65984fb9.jpg" width="200" /></a> <a href="http://tool.bitefu.net/pay/wxpay.png" target="_blank"><img height="274" src="https://oscimg.oschina.net/oscnet/1118495c283edf13318ed999c1ad23c1614.jpg" width="200" /></a> <a href="http://tool.bitefu.net/pay/alipay.jpg" target="_blank"><img height="272" src="https://oscimg.oschina.net/oscnet/1adfcc1bdf40d6cf35bcc80f94678d52e98.jpg" width="200" /></a>